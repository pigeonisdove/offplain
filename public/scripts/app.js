// Reload appcache when updated
if (window.applicationCache) {
  window.applicationCache.addEventListener('updateready', function() {
    if (confirm('An update is available. Reload now?')) {
      // window.location.reload(); // harsh
      window.applicationCache.swapCache();
    }
  });
}

// Save textbox content
function saveMessage(){
	var message = document.getElementById("draft");
	localStorage.setItem("message", message.value)
}
setInterval(saveMessage, 500);

window.addEventListener("DOMContentLoaded", loadMessage, false);

function loadMessage(){
	var textbox = document.getElementById("draft");
	var message = localStorage.getItem("message");

	 if (!message) {
		textbox.value = "";
	 }else {
		textbox.value = message;
	}
}

// from https://github.com/wildhaber/offline-first-sw

if('serviceWorker' in navigator) {

/**
 * Define if <link rel='next|prev|prefetch'> should
 * be preloaded when accessing this page
 */
const PREFETCH = true;

/**
 * Define which link-rel's should be preloaded if enabled.
 */
const PREFETCH_LINK_RELS = ['index','next', 'prev', 'prefetch'];

/**
 * prefetchCache
 */
function prefetchCache() {
    if(navigator.serviceWorker.controller) {

	let links = document.querySelectorAll(
	    PREFETCH_LINK_RELS.map((rel) => {
		return 'link[rel='+rel+']';
	    }).join(',')
	);

	if(links.length > 0) {
	    Array.from(links)
		.map((link) => {
		    let href = link.getAttribute('href');
		    navigator.serviceWorker.controller.postMessage({
			action : 'cache',
			url : href,
		    });
		});
	}


    }
}

/**
 * Register Service Worker
 */
navigator.serviceWorker
    .register('/scripts/sw.js', { scope: '/' })
    .then(() => {
	console.log('Service Worker Registered');
    });

/**
 * Wait if ServiceWorker is ready
 */
navigator.serviceWorker
    .ready
    .then(() => {
	if(PREFETCH) {
	    prefetchCache();
	}
    });

}
